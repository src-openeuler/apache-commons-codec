Name:           apache-commons-codec
Version:        1.11
Release:        7
Summary:        Apache common encoders and decoders
License:        ASL 2.0
URL:            https://commons.apache.org/proper/commons-codec/
Source0:        https://archive.apache.org/dist/commons/codec/source/commons-codec-%{version}-src.tar.gz

BuildArch:      noarch
BuildRequires:  maven-local mvn(org.apache.commons:commons-parent:pom:)
BuildRequires:  mvn(org.apache.maven.plugins:maven-assembly-plugin)

%description
Apache Commons Codec (TM) software provides implementations of common
encoders and decoders such as Base64, Hex, Phonetic and URLs.

%package        help
Summary:        Documents for apache-common-codec

Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description    help
The apache-commons-codec-help package contains related documents.

%prep
%autosetup -n commons-codec-%{version}-src

%mvn_file :     commons-codec %{name}
%mvn_alias :    commons-codec:commons-codec

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE.txt

%files help -f .mfiles-javadoc
%doc RELEASE-NOTES* NOTICE.txt

%changelog
* Thu Nov 07 2024 shaojiansong <shaojiansong@kylinos.cn> - 1.11-7
- Update project url and source url

* Tue Dec 03 2019 Jiangping Hu <hujiangping@huawei.com> - 1.11-6
- Package init
